# -*- coding: utf-8 -*-
from BeautifulSoup import BeautifulSoup
from SERP.items import SerpItem
from urlparse import urlparse, parse_qs

import scrapy
import csv
import re


class SerperSpider(scrapy.Spider):
    name = "Serper"

    def __init__(self, domain='', *args, **kwargs):
        super(SerperSpider, self).__init__(*args, **kwargs)
        self.template_uri = ('https://www.google.com/search?'
                             'q=site:{0}+%27.sg%27')
        self.start_urls = []
        self.domain = domain
        self.domain_re = re.compile(r'\S+\.sg')
        self.start_urls = [self.template_uri.format(domain)]
        self.base = 'https://www.google.com{0}'
        self.current_page = 1
        self.next_pages = []
        self.domain_items = []
        self.found_domains = []

    def parse(self, response):
        html = BeautifulSoup(response.body)
        urls = []
        for span in html.findAll('span', attrs={'class': 'st'}):
            span_text = span.text
            for found in self.domain_re.findall(span_text):
                urls.append(found)
        for url in set(urls):
            if url not in self.found_domains:
                self.found_domains.append(url)
                item = SerpItem()
                item['Domain'] = url
                yield item
        if self.current_page == 1:
            self.current_page += 1
            div_table = html.find('div', attrs={'id': 'foot'})
            nav_table = div_table.find('table')
            if nav_table:
                tr = nav_table.find('tr')
                if tr:
                    for td in tr.findAll('td')[1:]:
                        for alink in td.findAll('a', attrs={'class': 'fl'}):
                            self.next_pages.append(
                                                self.base.format(alink['href'])
                                                )
            for next_page in self.next_pages[1:-1]:
                self.logger.debug('Next Page %s', next_page)
                yield scrapy.Request(next_page, self.parse)
