# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem
import re


class SerpPipeline(object):
    ALLOWED_DOMAIN_NAME_LEN = 8
    re_dot = re.compile(r'\.')
    re_initial_nums_and_dot = re.compile(r'^\d+\.')
    re_html_escape_tags = re.compile(r'\d|&#?\d+;')
    re_three_dots = re.compile(r'\.{2,}')

    def process_item(self, item, spider):
        domain = item.get('Domain', None)
        if domain is not None:
            if not self.re_dot.findall(domain):
                raise DropItem('Invalid domain {0}'.format(item))
            else:
                domain = re.sub(self.re_initial_nums_and_dot, '', domain)
                domain = re.sub(self.re_html_escape_tags, '', domain)
                domain = re.sub(self.re_three_dots, '', domain)
                if len(domain) > self.ALLOWED_DOMAIN_NAME_LEN:
                    item['Domain'] = domain
                    return item
                else:
                    raise DropItem('Invalid domain {0} with length {1}'.format(
                        item, len(domain)
                    ))
